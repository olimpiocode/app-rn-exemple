import { LOGIN } from "../actions/auth";
import { SIGNUP } from "../actions/auth";

const initialState = {
    token: null,
    userId: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                token: action.token,
                userId: action.userId
            };
        case SIGNUP:
            return {
                token: action.token,
                userId: action.userId
            };  
        default: 
            return state;
    }
};

// facebook: https://rn-complete-guide-30a13.firebaseapp.com/__/auth/handler