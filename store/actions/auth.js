export const SIGNUP = 'SIGNUP';
export const LOGIN = 'LOGIN';

export const signup = (email, password) => {
    return async dispatch => {
        const response = await fetch(
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCdLhhoFXPnCcQYu73sJQDBu4L8xRlf-uo'
            ,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                    returnSecureToken: true
                })
            }
        );

        if (!response.ok) {
            const errorResData = await response.json();
            const errorId = errorResData.error.message;
            let message = 'Algo deu errado!';
            if (errorId === 'EMAIL_EXISTS') {
                message = 'Este e-mail já existe!';
            }
            throw new Error(message);
        }

        const resData = await response.json();
        console.log(resData);
        dispatch({ type: SIGNUP, token: resData.idToken, userId: resData.localId})
    };
};


export const login = (email, password) => {
    return async dispatch => {
        const response = await fetch(
            'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCdLhhoFXPnCcQYu73sJQDBu4L8xRlf-uo',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                    returnSecureToken: true
                })
            }
        );

        if (!response.ok) {
            const errorResData = await response.json();
            const errorId = errorResData.error.message;
            let message = 'Algo deu errado!';
            if (errorId === 'EMAIL_NOT_FOUND') {
                message = 'Não foi possível encontrar este email!';
            } else if (errorId === 'INVALID_PASSWORD') {
                message = 'Esta senha não é válida!';
            }
            throw new Error(message);
        }

        const resData = await response.json();
        console.log(resData);
        dispatch({ type: LOGIN, token: resData.idToken, userId: resData.localId})
    };
};