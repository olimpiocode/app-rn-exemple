import React, { useState, useEffect, useReducer, useCallback } from 'react';
import { 
    ScrollView,
    View,
    KeyboardAvoidingView,
    StyleSheet,
    Button,
    Text,
    ActivityIndicator,
    Alert
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useDispatch } from 'react-redux';

import Input from '../../components/UI/Input';
import Card from '../../components/UI/Card';
import Colors from '../../constants/Colors';
import * as authActions from '../../store/actions/auth';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value
      };
      const updatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid
      };
      let updatedFormIsValid = true;
      for (const key in updatedValidities) {
        updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
      }
      return {
        formIsValid: updatedFormIsValid,
        inputValidities: updatedValidities,
        inputValues: updatedValues
      };
    }
    return state;
  };

const AuthScreen = props => {
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [isSignup, setIsSignup] = useState(false);
    const dispatch = useDispatch();

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            email: '',
            password: ''
        },
        inputValidities: {
            email: false,
            password: false
        },
        formIsValid: false
    });

    useEffect(() => {
        if (error) {
            Alert.alert('An Error Occurred!', error, [{ text: 'Okay' }])
        }
    }, [error])

    const authHandler = async () => {
        let action;
        if (isSignup) {
            action = authActions.signup(
                formState.inputValues.email, 
                formState.inputValues.password
            );
        } else {
            action = authActions.login(
                formState.inputValues.email, 
                formState.inputValues.password
            );
        }
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(action);
            props.navigation.navigate('Shop');
        } catch(err) {
            setError(err.message);
            setIsLoading(false);
        }
    }

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
          });
        },
        [dispatchFormState]
    );

    return(
        <KeyboardAvoidingView 
        behavior='padding'
        keyboardVerticalOffset={50} 
        style={styles.screen}>
            <LinearGradient colors={['#008B8B', 'rgba(255,255,0, 0.5)']} style={styles.gradient}>
                <Card style={styles.authContainer}>
                    <ScrollView>
                        <Input
                            id="email"
                            label="E-Mail"
                            keyboardType="email-address"
                            required
                            email
                            autoCapitalize="none"
                            errorText="Por favor insira um endereço de e-mail válido." 
                            onInputChange={inputChangeHandler}
                            initialValue=""
                        />
                        <Input
                            id="password"
                            label="Senha"
                            keyboardType="default"
                            secureTextEntry
                            required
                            minLength={5}
                            autoCapitalize="none"
                            errorText="Por favor coloque uma senha válida." 
                            onInputChange={inputChangeHandler}
                            initialValue=""
                        />
                        <View style={styles.buttonContainer}>
                            { isLoading ? (<ActivityIndicator size="small" color={Colors.primary} />) : 
                                (<Button
                                    title={isSignup ? 'Inscrever-se' : 'Conecte-se'}
                                    color={Colors.primary}
                                    onPress={authHandler}
                                />)
                            }
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button 
                                title={`Troque para ${isSignup ? 'Conecte-se' : 'Inscrever-se'}`} 
                                color={Colors.accent} 
                                onPress={() => {
                                    setIsSignup(prevState => !prevState);
                                }} 
                            />
                        </View>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>{isSignup ? 'Inscrever-se' : 'Conecte-se'} com</Text>
                        </View>
                        <View style={styles.buttonsRedsContainer}>
                            <View style={styles.buttonReds}>
                                <Button title="Facebook"/>
                            </View>
                            <View style={styles.buttonReds}>
                                <Button title="Google"/>
                            </View>
                        </View>
                    </ScrollView>
                </Card>
            </LinearGradient>
        </KeyboardAvoidingView>
    )
};

AuthScreen.navigationOptions = {
    headerTitle: 'Autenticar'
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    authContainer: {
        width: '90%',
        maxWidth: 450,
        maxHeight: 450,
        padding: 20,
        backgroundColor: '#fff'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonContainer: {
        marginTop: 10
    },
    buttonsRedsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%'
    },
    buttonReds: {
        width: '33%'
    },
    textContainer: {
        marginVertical: 10,
        alignItems: 'center'
    },
    text: {
        color: Colors.primary,
        fontWeight: 'bold'
    }
});

export default AuthScreen;
